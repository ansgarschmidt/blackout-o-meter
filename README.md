# blackout-o-meter

Simple hardware display to show to current state (as frequency) for the European
power grid. Print, CNC or laser the blackout.svg, attach an WS2812B LED stripe
and run platformio to program an ESP2866.

The open data is sponsored by https://www.netfrequenzmessung.de, thank you very
much for this.

![Blackout-o-Meter](https://gitlab.com/ansgarschmidt/blackout-o-meter/raw/master/blackout-o-meter.png "Blackout-o-Meter")
