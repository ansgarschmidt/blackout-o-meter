#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

#define AP_SSID      "Motionlab-Guest"
#define AP_PASSWORD  "motionlab.guest"
#define INTENSITY    0.1
//                             0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20    21    22    23    24    25    26    27    28    29    30    31    32    33    34    35    36    37    38    39    40    41    42    43    44    45    46    47    48    49    50    51    52    53    54    55    56    57    58    59
static uint8_t  RED[]   = {    0,    0,    0,    0,    0,  255,  255,  255,  255,  250,  247,  234,  221,  208,  195,  182,  169,  156,  143,  130,  117,  104,   91,   78,   65,   52,   39,   26,   13,    0,   13,   26,   39,   52,   65,   78,   91,  104,  117,  130,  143,  156,  169,  182,  195,  208,  221,  234,  247,  250,  255,  255,  255,    0,    0,    0,    0,    0,    0,    0};
static uint8_t  GREEN[] = {    0,    0,    0,    0,    0,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,  255,    0,    0,    0,    0,    0,    0,    0};
static uint8_t  BLUE[]  = {  255,  255,  255,  255,  255,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,  255,  255,  255,  255,  255,  255,  255};
static uint16_t FREQ[]  = {47000,47500,48400,48700,49000,49400,49500,49600,49700,49800,49900,49910,49920,49930,49940,49950,49960,49970,49980,49990,49991,49992,49993,49994,49995,49996,49997,49998,49999,50000,50001,50002,50003,50004,50005,50006,50007,50008,50009,50010,50020,50030,50040,50050,50060,50070,50080,50090,50100,50200,50300,50400,50500,50600,50700,50800,50900,51000,51500,52000};

WiFiClient    client;
CRGBArray<60> leds;

void setup() {
  Serial.begin(9600);
  delay(300);
  Serial.print("Connecting to AP");
  WiFi.begin(AP_SSID, AP_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("DONE");
  FastLED.addLeds<WS2812B, D1, GRB>(leds, 60);
  leds[29] = CRGB(255 * INTENSITY, 255 * INTENSITY, 255 * INTENSITY);
}

uint16_t getFrequency(void){
  HTTPClient http;
  http.begin("http://www.netzfrequenzmessung.de/replay.php?nr=XXX");
  http.addHeader("Content-Type", "text/plain");
  uint8_t  httpCode = http.GET();
  uint16_t frequency = 50000;

  if(httpCode == HTTP_CODE_OK) {
    String  response  = http.getString();
    String  intpart   = response.substring(7,9);
    String  floatpart = response.substring(10,18);
    uint8_t fio       = floatpart.indexOf("<");
            floatpart = floatpart.substring(0,fio);

    while(floatpart.length() < 3){
        floatpart += "0";
    }

    frequency = (intpart+floatpart).toInt();

    if (frequency < 47000 || frequency > 52000){
        frequency = 50000;
    }
  }

  http.end();
  return frequency;
}

void setLeds(void){
    uint16_t f = getFrequency();
    Serial.println(f);

    if(f > 50000){

        for(uint8_t i = 0; i < 29; i++){
            leds[i] = CRGB(     0,        0,       0);
        }

        for(uint8_t i = 30; i < 60; i++){
            if( f >= FREQ[i] ){
                leds[i] = CRGB(RED[i] * INTENSITY, GREEN[i] * INTENSITY, BLUE[i] * INTENSITY);
            }else{
                leds[i] = CRGB(     0,        0,       0);
            }
        }
    }else{
        for(uint8_t i = 30; i < 60; i++){
            leds[i] = CRGB(     0,        0,       0);
        }

        for(uint8_t i = 28; i > 0; i--){
            if( f <= FREQ[i] ){
                leds[i] = CRGB(RED[i] * INTENSITY, GREEN[i] * INTENSITY, BLUE[i] * INTENSITY);
            }else{
                leds[i] = CRGB(     0,        0,       0);
            }
        }

    }
}

void loop() {
    setLeds();
    FastLED.delay(1000);
}
